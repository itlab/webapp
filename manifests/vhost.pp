# @summary creates Apache2 vhosts
#
# @example Basic Usage
#   class { 'webapp::vhost':
#     ensure => present,
#   }
# 
# @param ensure
#   whether to include this vhost
# @param log_dir
#   directory for apache logs
# @param ssl_verify_client
#   whether to verify client certificates
# @param ssl_verify_depth
#   how far up the chain to verify
# @param custom_fragment
#   extra configuration
# @param priority
#   vhost priority
# @param log_level
#   log detail level
#
define webapp::vhost (
  Enum['absent', 'present'] $ensure = 'present',
  String $log_dir = '/var/log/apache2',
  String $ssl_verify_client = 'none',
  Integer $ssl_verify_depth = 5,
  Optional[String] $custom_fragment = undef,
  Integer $priority = 20,
  String $log_level = 'debug',
) {
  apache::vhost { $name:
    ensure               => $ensure,
    priority             => $priority,
    port                 => 443,
    ssl                  => true,
    ssl_honorcipherorder => 'on',
    ssl_protocol         => ['-all', '+TLSv1.2', '+TLSv1.3'],
    ssl_cert             => "/etc/apache2/lego/certificates/${name}.itlab.stanford.edu.crt",
    ssl_chain            => "/etc/apache2/lego/certificates/${name}.itlab.stanford.edu.issuer.crt",
    ssl_key              => "/etc/apache2/lego/certificates/${name}.itlab.stanford.edu.key",
    ssl_ca               => '/etc/apache2//ca-client.pem',
    ssl_verify_client    => $ssl_verify_client,
    ssl_verify_depth     => $ssl_verify_depth,
    ssl_options          => [
      '+ExportCertData',
      '+StdEnvVars',
      '+OptRenegotiate',
    ],
    ssl_cipher           => 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384',
    docroot              => "/var/www/${name}",
    docroot_owner        => 'root',
    docroot_group        => 'www-data',
    docroot_mode         => '0755',
    directoryindex       => 'index.html index.php',
    servername           => "${name}.itlab.stanford.edu",
    access_log_file      => "${log_dir}/${name}-access.log",
    access_log_format    => 'vhost_combined',
    log_level            => $log_level,
    error_log_file       => "${log_dir}/${name}-error.log",
    error_documents      => [
      {
        'error_code' => '401',
        'document'   => '/401/index.php'
      },
    ],
    request_headers      => [
      'set SSL_CLIENT_CERT "%{SSL_CLIENT_CERT}s"',
      'set SSL_CIPHER "%{SSL_CIPHER}s"',
      'set SSL_SESSION_ID "%{SSL_SESSION_ID}s"',
      'set SSL_CIPHER_USEKEYSIZE "%{SSL_CIPHER_USEKEYSIZE}s"',
      'set x-ssl-s-dn "%{SSL_CLIENT_S_DN}e"',
      'set x-ssl-i-dn "%{SSL_CLIENT_I_DN}e"',
      'set x-ssl-cert "%{SSL_CLIENT_CERT}e"',
      'set x-ssl-client-verify "%{SSL_CLIENT_VERIFY}e"',
      'set x-client-port "%{REMOTE_PORT}e"',
    ],

    protocols            => ['http/1.1','h2'],
    custom_fragment      => $custom_fragment,
    aliases              => [
      {
        alias => '/shibboleth-sp',
        path  => '/usr/share/shibboleth',
      },
    ],
    directories          => [
      {
        path           => '/',
        allow_override => ['none'],
        options        => ['FollowSymLinks'],
      },
      {
        path           => "/var/www/${name}",
        allow_override => ['none'],
#        authz_core     => {
#          require => ['all granted'],
#        },
      },
      {
        path     => '.+\.php$',
        provider => 'filesmatch',
        handler  => '"proxy:unix:/var/run/php/php8.2-fpm.sock|fcgi://localhost/"',
      }
    ],
  }
}
