# Configure the HTTP to HTTPS redirect
#
class webapp::redirect {
  apache::vhost { "${name}-redirect":
    port            => 80,
    ssl             => false,
    docroot         => '/var/www',
    serveradmin     => 'admin@itlab.stanford.edu',
    access_log_file => '/var/log/apache2/access.log',
    error_log_file  => '/var/log/apache2/error.log',
    rewrites        => [
      {
        comment      => 'handle ACME Challenge requests',
        rewrite_cond => [
          '%{REQUEST_METHOD} ^(GET|HEAD|OPTIONS|POST)$',
          '%{HTTP_HOST} ^.+\.itlab\.stanford\.edu$',
        ],
        rewrite_rule => [
          '^/.well-known/acme-challenge/(.*) /var/www/.well-known/acme-challenge/$1 [L]',
        ],
      },
      {
        comment      => 'redirect to HTTPS',
        rewrite_cond => [
          '%{REQUEST_METHOD} ^(GET|HEAD|OPTIONS|POST)$',
          '%{HTTP_HOST} ^.+\.itlab\.stanford\.edu$',
        ],
        rewrite_rule => [
          '^/(.*) https://%{HTTP_HOST}/$1 [R=301,L]',
        ],
      },
      {
        rewrite_rule => ['. - [F,L]'],
      }
    ],
  }
}
