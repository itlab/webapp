# @summary configure webapp
#
# @param vhosts - vhost data (from Hiera)
#
class webapp (
  Hash $vhosts,
) {
  file {
    [
      '/usr/local/bin',
      '/etc/lego',
      '/etc/apache2/cacerts-enabled',
    ]:
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }

  file { '/etc/apache2/cacerts-available':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => "puppet:///modules/${module_name}/cacerts",
    recurse => true,
  }

  file { '/etc/lego/accounts':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => '/efs/lego/accounts',
    require => Nfs::Client::Mount['/efs'],
    recurse => true,
  }

  file { '/usr/local/bin/dns-update':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => "puppet:///modules/${module_name}/dns-update",
    require => File['/usr/local/bin'],
  }

  file { '/etc/cron.daily/lego':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => "puppet:///modules/${module_name}/lego-cron",
  }

  file { '/usr/local/bin/lego-init':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => "puppet:///modules/${module_name}/lego-init",
    require => File['/usr/local/bin'],
  }

  systemd::manage_unit { 'dns-update.service':
    unit_entry    => {
      'Description' => 'Update Route53 DNS Record',
      'After'       => [
        'cloud-init-local.service',
        'networking.service',
        'network-online.target',
      ],
    },
    service_entry => {
      'Type'            => 'oneshot',
      'ExecStart'       => '/usr/local/bin/dns-update',
      'RemainAfterExit' => 'yes',
      'TimeoutSec'      => '0',
      'StandardOutput'  => 'journal+console',
    },
    install_entry => {
      'WantedBy' => 'multi-user.target',
    },
    enable        => true,
    active        => true,
    require       => File['/usr/local/bin/dns-update'],
  }

  nfs::client::mount { '/efs':
    ensure      => 'mounted',
    server      => 'fs-85ab5b2c.efs.us-west-2.amazonaws.com',
    share       => '/',
    options_nfs => '_netdev,nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2',
    atboot      => true,
  }

  package {
    [
      'lego',
      'tcpdump',
      'man-db',
      'manpages',
      'php-cli',
      'php-curl',
      'php-fpm',
      'php-json',
      'php-opcache',
    ]:
      ensure => 'present',
  }

  exec { 'lego-init':
    command     => '/usr/local/bin/lego-init',
    user        => 'root',
    refreshonly => true,
    require     => [
      File['/usr/local/bin/lego-init'],
    ],
  }

  exec { 'link cacerts':
    command => 'ln -s /etc/apache2/cacerts-available/*',
    cwd     => '/etc/apache2/cacerts-enabled',
    require => [
      File['/etc/apache2/cacerts-available'],
      File['/etc/apache2/cacerts-enabled'],
    ],
  }

  exec { 'rehash cacerts':
    command => 'c_rehash .',
    cwd     => '/etc/apache2/cacerts-enabled',
    require => [
      Exec['link cacerts'],
    ],
  }

  class { 'webapp::apache': }
  class { 'webapp::shib': }
}
