# @summary configure webapp
#
# @param vhosts - vhost data (from Hiera)
#
class webapp::apache (
  Hash $vhosts,
) {
  class { 'apache':
    service_enable      => false,
    service_ensure      => false,
    service_manage      => false,
    servername          => 'webapp.itlab.stanford.edu',
    serveradmin         => 'admin@itlab.stanford.edu',
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      vhost_common => '%v %h %l %u %t \"%r\" %>s %b',
      combined_elb => '%v %{c}a %a %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"',
    },
  }

  class {
    [
      'apache::mod::alias',
      'apache::mod::auth_basic',
      'apache::mod::authn_core',
      'apache::mod::authz_core',
      'apache::mod::cgi',
      'apache::mod::cgid',
      'apache::mod::headers',
      'apache::mod::mime',
      'apache::mod::mime_magic',
      'apache::mod::negotiation',
      'apache::mod::proxy_fcgi',
      'apache::mod::proxy_http',
      'apache::mod::proxy_http2',
      'apache::mod::proxy_wstunnel',
      'apache::mod::remoteip',
      'apache::mod::rewrite',
      'apache::mod::remoteip',
      'apache::mod::socache_shmcb',
    ]:
  }

  class { 'apache::mod::http2':
    h2_push => false,
  }

  class { 'apache::mod::fcgid':
    options => {
      'FcgidIPCDir'  => '/var/run/fcgidsock',
      'SharememPath' => '/var/run/fcgid_shm',
      'AddHandler'   => 'fcgid-script .fcgi',
    },
  }

  class { 'apache::mod::dir':
    indexes => ['index.html', 'index.php'],
  }
  class { 'apache::mod::shib':
    package_name => 'libapache2-mod-shib',
  }

  class { 'apache::mod::ssl':
    ssl_honorcipherorder       => true,
    ssl_stapling               => true,
    stapling_cache             => 'shmcb:logs/ssl_stapling(32768)',
    ssl_stapling_return_errors => false,
  }

  create_resources(webapp::vhost, $vhosts)
}
