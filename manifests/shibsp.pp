# Create an ApplicationOverride for an SP
#
# @param entity_id SP's entity ID
# @param idp_entity_id IdP's entity ID
# @param authn_context SAML Authentication Context
# @param authn_context_comp SAML Authentication Context Comparison
# @param force_authn request forced authentication if true
# @param passive_authn request passive authentication if true
class webapp::shibsp (
  String $entity_id,
  String $idp_entity_id = undef,
  String $authn_context = undef,
  String $authn_context_comp = 'exact',
  Boolean $force_authn = false,
  Boolean $passive_authn = false,
) {
  file { '/etc/shibboleth/sps/$name.xml':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template("${module_name}/shibsp.erb"),
  }
}
