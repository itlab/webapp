# Configure Shibboleth
#
# @param shibsps Hiera data for SP definitions
#
class webapp::shib (
  Hash $shibsps,
) {
  file { '/etc/shibboleth':
    ensure  => 'directory',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => "puppet:///modules/${module_name}/shibboleth",
    recurse => true,
  }

  file { '/etc/shibboleth/sps':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  create_resources(webapp::shibsp, $shibsps)
}
