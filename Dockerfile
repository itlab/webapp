FROM suet/shibsp:latest
LABEL maintainer="swl@stanford.edu"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qq update \
    && apt-get -qq -y --no-install-recommends install php

VOLUME /etc/apache2/sites-available
VOLUME /etc/apache2/sites-enabled


